package itis.parsing;

import itis.parsing.annotations.FieldName;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        //write your code here
        File file = new File(parkDatafilePath);
        try {
            Park park = null;
            Class parkClass = Class.forName(Park.class.getName());
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bufferedReader.readLine();
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != "***"){
                String nameOfField = currentLine.substring(1, (currentLine.indexOf(":") - 1));
                String value = currentLine.substring(currentLine.indexOf(":") +1).replace('\"', ' ').trim();
                Field[] fields = parkClass.getDeclaredFields();
                Arrays.stream(fields).filter(field -> {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(FieldName.class) &&
                            field.getAnnotation(FieldName.class).value() == nameOfField){
                        try {
                            field.set(parkClass, value);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else if (field.getName() == nameOfField){
                        try {
                            field.set(parkClass, value);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                    return false;
                });
                return (Park) parkClass.newInstance();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
